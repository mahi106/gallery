const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Gallery = new Schema({
    url: { type: String, required: true },
    title: { type: String },
    location: { type: String },
    tags: { type: [String], required: true }
}, {
    collection: 'gallery'
});

module.exports = mongoose.model('Gallery', Gallery);

